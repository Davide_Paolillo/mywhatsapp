package mobile.uni.mywhatsapp.Chat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import mobile.uni.mywhatsapp.User.UserObject;

public class ChatObject implements Serializable {

    private String chatId;

    private String chatName = "";

    private String lastMsg = "";

    private String profileImage = "";

    private int visibility = 0;

    private boolean isGroup = false;

    private ArrayList<UserObject> userObjectArrayList = new ArrayList<>();

    public ChatObject(String chatId){
        this.chatId = chatId;
    }

    public String getChatId() {
        return chatId;
    }

    public ArrayList<UserObject> getUserObjectArrayList() {
        return userObjectArrayList;
    }

    public void addUserToArrayList(UserObject mUser){
        userObjectArrayList.add(mUser);
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public void setLastMsg(String lastMsg, int visibility){
        this.lastMsg = lastMsg;
        this.visibility = visibility;
    }

    public String getChatName() {
        return chatName;
    }

    public String getLastMsg(){
        return lastMsg;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public boolean isGroup() {
        return isGroup;
    }
}
