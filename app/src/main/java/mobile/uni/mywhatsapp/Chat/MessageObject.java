package mobile.uni.mywhatsapp.Chat;

import java.util.ArrayList;

public class MessageObject {

    String messageId, message, senderId, senderImage;

    ArrayList<String> mediaUrlList;

    public MessageObject(String messageId, String message, String senderId, ArrayList<String> mediaUrlList, String senderImage){
        this.messageId = messageId;
        this.message = message;
        this.senderId = senderId;
        this.mediaUrlList = mediaUrlList;
        this.senderImage = senderImage;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getSenderId() {
        return senderId;
    }

    public ArrayList<String> getMediaUrlList() {
        return mediaUrlList;
    }

    public String getSenderImage() {
        return senderImage;
    }
}
