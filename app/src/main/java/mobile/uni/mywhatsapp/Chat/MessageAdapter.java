package mobile.uni.mywhatsapp.Chat;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.uni.mywhatsapp.R;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {


    private ArrayList<MessageObject> messageList;
    private View layoutView;

    public MessageAdapter(ArrayList<MessageObject> messageList){
        this.messageList = messageList;
    }

    @NonNull
    @Override
    public MessageAdapter.MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_message, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutView.setLayoutParams(lp);

        MessageAdapter.MessageViewHolder rcv = new MessageAdapter.MessageViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageAdapter.MessageViewHolder MessageViewHolder, final int i) {
        if(!messageList.get(i).getSenderImage().equals("")){
            Picasso.get().load(messageList.get(i).getSenderImage()).into(MessageViewHolder.mProfileImage);
        } else {
            MessageViewHolder.mProfileImage.setImageResource(R.drawable.ic_person_black_24dp);
        }

        if(messageList.get(i).getSenderId().equals(FirebaseAuth.getInstance().getUid())){
            MessageViewHolder.mMessageReceived.setVisibility(View.GONE);
            MessageViewHolder.mMsgSend.setVisibility(View.VISIBLE);
            MessageViewHolder.mSender.setVisibility(View.VISIBLE);
            MessageViewHolder.mSender.setText(messageList.get(i).getMessage());
        } else {
            MessageViewHolder.mMsgSend.setVisibility(View.GONE);
            MessageViewHolder.mMessageReceived.setVisibility(View.VISIBLE);
            MessageViewHolder.mMessageReceived.setVisibility(View.VISIBLE);
            MessageViewHolder.mMessageReceived.setText(messageList.get(i).getMessage());
        }

        if(!messageList.get(MessageViewHolder.getAdapterPosition()).getMediaUrlList().isEmpty()){
            if(messageList.get(MessageViewHolder.getAdapterPosition()).getSenderId().equals(FirebaseAuth.getInstance().getUid())){
                MessageViewHolder.mViewMedia.setVisibility(View.VISIBLE);
                if( messageList.get(MessageViewHolder.getAdapterPosition()).getMediaUrlList().size() > 1){
                    MessageViewHolder.mImageNumber.setVisibility(View.VISIBLE);
                    MessageViewHolder.mImageNumber.setText("+" + Integer.toString(messageList.get(i).getMediaUrlList().size()));
                }
                Picasso.get().load(messageList.get(i).getMediaUrlList().get(0)).into(MessageViewHolder.mViewMedia);
                MessageViewHolder.mViewMedia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new ImageViewer.Builder(v.getContext(), messageList.get(i).getMediaUrlList())
                                .setStartPosition(0)
                                .show();
                    }
                });
            } else {
                MessageViewHolder.mViewMediaReceived.setVisibility(View.VISIBLE);
                if( messageList.get(MessageViewHolder.getAdapterPosition()).getMediaUrlList().size() > 1){
                    MessageViewHolder.mImageNumberReceived.setVisibility(View.VISIBLE);
                    MessageViewHolder.mImageNumberReceived.setText("+" + Integer.toString(messageList.get(i).getMediaUrlList().size()));
                }
                Picasso.get().load(messageList.get(i).getMediaUrlList().get(0)).into(MessageViewHolder.mViewMediaReceived);
                MessageViewHolder.mViewMediaReceived.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new ImageViewer.Builder(v.getContext(), messageList.get(i).getMediaUrlList())
                                .setStartPosition(0)
                                .show();
                    }
                });
            }
        } else {
            MessageViewHolder.mViewMediaReceived.setVisibility(View.GONE);
            MessageViewHolder.mImageNumberReceived.setVisibility(View.GONE);
            MessageViewHolder.mViewMedia.setVisibility(View.GONE);
            MessageViewHolder.mImageNumber.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }


    class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView mSender, mImageNumber, mMessageReceived, mImageNumberReceived;
        ImageView mViewMedia, mViewMediaReceived;
        CircleImageView mProfileImage;
        LinearLayout mLayout, mMsgSend, mMsgReceived;
        MessageViewHolder(View view){
            super(view);
            mSender = view.findViewById(R.id.sender);
            mLayout = view.findViewById(R.id.layout);
            mMsgSend = view.findViewById(R.id.msgSend);
            mMsgReceived = view.findViewById(R.id.msgReceived);
            mViewMedia = view.findViewById(R.id.viewMedia);
            mImageNumber = view.findViewById(R.id.imgNumber);
            mMessageReceived = view.findViewById(R.id.message_recieved);
            mImageNumberReceived = view.findViewById(R.id.imgNumberReceived);
            mViewMediaReceived = view.findViewById(R.id.viewMediaReceived);
            mProfileImage = view.findViewById(R.id.message_profile_img);
        }
    }
}
