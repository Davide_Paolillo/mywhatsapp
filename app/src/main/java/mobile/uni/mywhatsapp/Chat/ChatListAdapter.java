package mobile.uni.mywhatsapp.Chat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.uni.mywhatsapp.Activities.ChatActivity;
import mobile.uni.mywhatsapp.R;
import mobile.uni.mywhatsapp.User.UserObject;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ChatListViewHolder> {


    private ArrayList<ChatObject> chatList;
    private static ArrayList<CheckBox> checkBoxes;

    public ChatListAdapter(ArrayList<ChatObject> chatList){
        this.chatList = chatList;
        checkBoxes = new ArrayList<>();
    }

    @NonNull
    @Override
    public ChatListAdapter.ChatListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        @SuppressLint("InflateParams") View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_chat, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutView.setLayoutParams(lp);
        CheckBox c = layoutView.findViewById(R.id.delChat);
        checkBoxes.add(c);

        return new ChatListViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatListAdapter.ChatListViewHolder ChatListViewHolder, final int i) {
        ChatListViewHolder.mTitle.setText(chatList.get(i).getChatName());
        ChatListViewHolder.mLastMsg.setText(chatList.get(i).getLastMsg());
        ChatListViewHolder.mPhotoMsg.setVisibility(chatList.get(i).getVisibility());
        if(!chatList.get(i).getProfileImage().equals("") && !chatList.get(i).isGroup()){
            Picasso.get().load(chatList.get(i).getProfileImage()).into(ChatListViewHolder.mProfileImg);
        }
        ChatListViewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ChatActivity.class);
                intent.putExtra("chatObject", chatList.get(ChatListViewHolder.getAdapterPosition()));
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    public static ArrayList<CheckBox> getCheckBoxes(){
        return checkBoxes;
    }

    class ChatListViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        CheckBox mCheckBox;
        LinearLayout mLayout;
        TextView mLastMsg;
        ImageView mPhotoMsg;
        CircleImageView mProfileImg;

        ChatListViewHolder(View view){
            super(view);
            mTitle = view.findViewById(R.id.title);
            mCheckBox = view.findViewById(R.id.delChat);
            mLayout = view.findViewById(R.id.layout);
            mProfileImg = view.findViewById(R.id.profile_img_contact);
            mLastMsg = view.findViewById(R.id.last_msg);
            mPhotoMsg = view.findViewById(R.id.photo_msg);
        }
    }
}
