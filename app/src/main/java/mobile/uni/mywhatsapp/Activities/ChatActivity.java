package mobile.uni.mywhatsapp.Activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.uni.mywhatsapp.Chat.ChatObject;
import mobile.uni.mywhatsapp.Chat.MediaAdapter;
import mobile.uni.mywhatsapp.Chat.MessageAdapter;
import mobile.uni.mywhatsapp.Chat.MessageObject;
import mobile.uni.mywhatsapp.R;
import mobile.uni.mywhatsapp.User.UserObject;
import mobile.uni.mywhatsapp.Utils.DataUtils;
import mobile.uni.mywhatsapp.Utils.SendNotification;

public class ChatActivity extends AppCompatActivity {

    private int PICK_IMAGE_INTENT = 1;
    private int totalMediaUploaded = 0;

    private RecyclerView.Adapter mChatAdapter;
    private RecyclerView.Adapter<MediaAdapter.MediaViewHolder> mMediaAdapter;
    private RecyclerView.LayoutManager mChatLayoutManager;

    private ArrayList<MessageObject> messageList;
    private ArrayList<String> mediaUriList = new ArrayList<>();
    private ArrayList<String> mediaIdList = new ArrayList<>();

    private EditText mMessage;

    ChatObject mChatObject;

    private ActionBar actionBar;

    private DatabaseReference mChatMessagesDb;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View actionBarLayout = getLayoutInflater().inflate(R.layout.my_profile_img, null);

        actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setCustomView(actionBarLayout);
        setContentView(R.layout.activity_chat);
        mChatObject = (ChatObject) getIntent().getSerializableExtra("chatObject");
        ImageButton mSend = findViewById(R.id.send);
        mSend.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
        ImageButton mAddMedia = findViewById(R.id.addMedia);
        mAddMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });
        mChatMessagesDb = FirebaseDatabase.getInstance().getReference().child("chat").child(mChatObject.getChatId()).child("messages");
        initializeMessage();
        initializeMedia();
        getChatMessages();
        setTitle();
    }

    private void getChatMessages() {
        mChatMessagesDb.addChildEventListener(new ChildEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if(dataSnapshot.exists()){
                    String text = "";
                    String creatorID = "";
                    ArrayList<String> mediaUrlList = new ArrayList<>();
                    if(dataSnapshot.child("text").getValue() != null){
                        text = Objects.requireNonNull(dataSnapshot.child("text").getValue()).toString();
                    }
                    if(dataSnapshot.child("creator").getValue() != null){
                        creatorID = Objects.requireNonNull(dataSnapshot.child("creator").getValue()).toString();
                    }
                    if(dataSnapshot.child("media").getChildrenCount() > 0){
                        for(DataSnapshot mediaSnapshot : dataSnapshot.child("media").getChildren()){
                            mediaUrlList.add(Objects.requireNonNull(mediaSnapshot.getValue()).toString());
                        }
                    }
                    MessageObject mMessage = new MessageObject(dataSnapshot.getKey(), text, creatorID, mediaUrlList, getCreatorImage(creatorID));
                    messageList.add(mMessage);
                    mChatLayoutManager.scrollToPosition(messageList.size()-1); // scroll until the end of the list, so we can see the last messages
                    mChatAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private String getCreatorImage(String creatorId) {
        return DataUtils.getContactImage(creatorId);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void sendMessage() {
        mMessage = findViewById(R.id.mes);
        String messageId = mChatMessagesDb.push().getKey();
        final DatabaseReference newMessageDB = mChatMessagesDb.child(Objects.requireNonNull(messageId));

        final Map<String, Object> newMessageMap = new HashMap<>();
        newMessageMap.put("creator", Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));

        if(!mMessage.getText().toString().isEmpty()){
            newMessageMap.put("text", mMessage.getText().toString());
        }

        if(!mediaUriList.isEmpty()){
            for(String mediaUri: mediaUriList){
                String mediaId = newMessageDB.child("media").push().getKey();
                mediaIdList.add(mediaId);
                final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("chat").child(mChatObject.getChatId()).child(messageId).child(Objects.requireNonNull(mediaId));

                UploadTask uploadTask = filePath.putFile(Uri.parse(mediaUri));
                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                newMessageMap.put("/media/" + mediaIdList.get(totalMediaUploaded) + "/" , uri.toString());
                                totalMediaUploaded++;
                                if(totalMediaUploaded == mediaUriList.size()){
                                    updateDbWithNewMessage(newMessageDB, newMessageMap);
                                }
                            }
                        });
                    }
                });
            }
        } else {
            if(!mMessage.getText().toString().isEmpty()){
                updateDbWithNewMessage(newMessageDB, newMessageMap);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void updateDbWithNewMessage (DatabaseReference newMessageDb, Map<String, Object> newMessageMap){
        newMessageDb.updateChildren(newMessageMap);
        mMessage.setText(null);
        mediaUriList.clear();
        mediaIdList.clear();
        mMediaAdapter.notifyDataSetChanged();
        String message;

        if(newMessageMap.get("text") != null){
            message = newMessageMap.get("text").toString();
        } else {
            message = "Sent media";
        }

        for(final UserObject mUser : mChatObject.getUserObjectArrayList()){
            if(!mUser.getUid().equals(FirebaseAuth.getInstance().getUid())){
                new SendNotification(message, "New Message", mUser.getNotificationKey());
            }
        }
    }

    private void initializeMedia() {
       mediaUriList = new ArrayList<>();
       RecyclerView mMedia = findViewById(R.id.mediaList);
       mMedia.setNestedScrollingEnabled(false);
       mMedia.setHasFixedSize(false);
       RecyclerView.LayoutManager mMediaLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayout.HORIZONTAL, false);
       mMedia.setLayoutManager(mMediaLayoutManager);
       mMediaAdapter = new MediaAdapter(getApplicationContext(), mediaUriList);
       mMedia.setAdapter(mMediaAdapter);
    }

    private void initializeMessage() {
        messageList = new ArrayList<>();
        RecyclerView mChat = findViewById(R.id.messageList);
        mChat.setNestedScrollingEnabled(false);
        mChat.setHasFixedSize(false);
        mChatLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayout.VERTICAL, false);
        mChat.setLayoutManager(mChatLayoutManager);
        mChatAdapter = new MessageAdapter(messageList);
        mChat.setAdapter(mChatAdapter);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*"); // way to say that user should pick an image
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Pictures"), PICK_IMAGE_INTENT);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == PICK_IMAGE_INTENT){
                if(Objects.requireNonNull(data).getClipData() == null){
                    mediaUriList.add(Objects.requireNonNull(data.getData()).toString());
                } else {
                    for(int i = 0; i < Objects.requireNonNull(data.getClipData()).getItemCount(); i++){
                        mediaUriList.add(data.getClipData().getItemAt(i).getUri().toString());
                    }
                }
                mMediaAdapter.notifyDataSetChanged();
            }
        }
    }

    private void setTitle(){
        DatabaseReference mChatDb = FirebaseDatabase.getInstance().getReference().child("chat").child(mChatObject.getChatId());
        mChatDb.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    final TextView mTextView = findViewById(R.id.actionBarText);
                    if(dataSnapshot.child("info").child("users").getChildrenCount() <= 2){
                        for(DataSnapshot childSnapshot : dataSnapshot.child("info").child("users").getChildren()){
                            if(!childSnapshot.getKey().equals(FirebaseAuth.getInstance().getUid())){
                                DatabaseReference mUserDb = FirebaseDatabase.getInstance().getReference().child("user").child(childSnapshot.getKey());
                                mUserDb.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()){
                                            if(!DataUtils.getNameFromPhone(dataSnapshot.child("phone").getValue().toString()).equals("")){
                                                mTextView.setText(DataUtils.getNameFromPhone(dataSnapshot.child("phone").getValue().toString()));
                                            } else {
                                                TextView mTextView = findViewById(R.id.actionBarText);
                                                mTextView.setText(dataSnapshot.child("phone").getValue().toString());
                                            }
                                            setContactImage(dataSnapshot);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                    } else {
                        mTextView.setText("New Group");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setContactImage(DataSnapshot dataSnapshot){
        if(dataSnapshot.hasChild("profileImg")){
            CircleImageView mCircleImgView = findViewById(R.id.profileImgActionBar);
            Picasso.get().load(dataSnapshot.child("profileImg").getValue().toString()).into(mCircleImgView);
        }
    }
}
