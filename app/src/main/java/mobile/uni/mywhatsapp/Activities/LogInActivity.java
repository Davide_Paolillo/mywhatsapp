package mobile.uni.mywhatsapp.Activities;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import mobile.uni.mywhatsapp.R;
import mobile.uni.mywhatsapp.Utils.CountryToPhonePrefixUtils;
import mobile.uni.mywhatsapp.Utils.DataUtils;
import mobile.uni.mywhatsapp.databinding.ActivityMainBinding;

public class LogInActivity extends AppCompatActivity {

    private ActivityMainBinding mBinding;

    private EditText mPhoneNumber, mCode;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private String mVerificationId;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseApp.initializeApp(this);
        new DataUtils();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mPhoneNumber = findViewById(R.id.phoneNumber);
        mCode = findViewById(R.id.code);
        FrameLayout mSend = findViewById(R.id.send);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getPermissions();
            FirebaseDatabase.getInstance().getReference().keepSynced(true);
            userIsLoggedIn();
            /* Button listener called when we clicked */
            mSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    load(v);
                }
            });

        /* Called at the end of the phone verification process, if it's successful
        execute a change of activity, otherwise show a error toast */
            mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                @Override
                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                    signInWithPhoneAuthCredential(phoneAuthCredential);
                }

                @Override
                public void onVerificationFailed(FirebaseException e) {
                    Toast.makeText(getApplicationContext(), "Sorry something went wrong, please retry", Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(getIntent());
                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onCodeSent(String verification, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                    super.onCodeSent(verification, forceResendingToken);
                    mVerificationId = verification;
                }
            };
        } else {
            Toast.makeText(getApplicationContext(), "Your API is too low, please update your system in order to use this app", Toast.LENGTH_LONG).show();
        }
    }

    private void getPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS}, 1);
        }
    }

    private void verifyPhoneNumberWithCode(){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, mCode.getText().toString());
        signInWithPhoneAuthCredential(credential);
    }

    /** Check if the user credentials are right, if so execute userIsLoggedIn function */
    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
        FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    if (user != null) {
                        final DatabaseReference mUserDB = FirebaseDatabase.getInstance().getReference().child("user").child(user.getUid());
                        mUserDB.addListenerForSingleValueEvent(new ValueEventListener() {
                            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(!dataSnapshot.exists()){
                                    Map<String, Object> userMap = new HashMap<>();
                                    userMap.put("phone", Objects.requireNonNull(user.getPhoneNumber()));
                                    userMap.put("name", user.getPhoneNumber());
                                    mUserDB.updateChildren(userMap);
                                }
                                userIsLoggedIn();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }
        });
    }

    /** Double check if the user is logged in, if so jump into the LoggedInActivity */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void userIsLoggedIn() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser(); // get the user who asked to log in
        if(user != null){
            animateButton();
            fadeOutTextAndSetProgressDialog();
            nextAction();
            //finish();
            /*startActivity(new Intent(getApplicationContext(), LoggedInActivity.class));*/
            // finish(); // close all open instances
        }
    }

    /** Use a Firebase function to verify the existence of the phone number taken in input */
    private void startPhoneNumberVerification() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(mPhoneNumber.getText().toString(), // transforming the phone number into a string
                60, // maximum time conceded to input the code
                TimeUnit.SECONDS, // time unit
                this,
                mCallbacks); // call back variable that executes the success or the failure of the authentication
    }

    @SuppressLint("SetTextI18n")
    public void load (View view){
        mPhoneNumber.getText().toString();
        if (!String.valueOf(mPhoneNumber.getText().toString().charAt(0)).equals("+")) {
            mPhoneNumber.getEditableText().insert(0, CountryToPhonePrefixUtils.getCountryISO(getApplicationContext()));
        }
        if (mVerificationId != null) {
            animateButton();
            fadeOutTextAndSetProgressDialog();
            verifyPhoneNumberWithCode();
        } else {
            mBinding.signInText.setText("Verify Code");
            startPhoneNumberVerification();
        }
    }

    private void animateButton() {
        ValueAnimator mAnimation = ValueAnimator.ofInt(mBinding.send.getMeasuredWidth(), getFinalWidth());
        mAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (Integer) animation.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = mBinding.send.getLayoutParams();
                layoutParams.width = value;
                mBinding.send.requestLayout();
            }
        });
        mAnimation.setDuration(250);
        mAnimation.start();
    }

    private void fadeOutTextAndSetProgressDialog(){
        mBinding.signInText.animate().alpha(0f).setDuration(250).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                showProgressDialog();
            }
        }).start();
    }

    private void showProgressDialog(){
        mBinding.progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);
        mBinding.progressBar.setVisibility(View.VISIBLE);
    }

    private void nextAction(){
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                revealButton();
                fadeOutProgressDialog();
                delayedStartNextActivity();
            }
        }, 3000);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealButton(){

         mBinding.revelView.setVisibility(View.VISIBLE);
         mBinding.revelView.setVisibility(View.VISIBLE);

         int x = mBinding.revelView.getWidth();
         int y = mBinding.revelView.getHeight();

         int startX = (int) (getFinalWidth() /2 + mBinding.send.getX());
         int startY = (int) (getFinalWidth() /2 + mBinding.send.getY());

         float radius = Math.max(x,y) * 1.2f;
         Animator reveal = ViewAnimationUtils.createCircularReveal(mBinding.revelView, startX, startY, getFinalWidth(), radius);
         reveal.setDuration(350);
         reveal.addListener(new AnimatorListenerAdapter() {
             @Override
             public void onAnimationEnd(Animator animation) {
                 super.onAnimationEnd(animation);
             }
         });
         reveal.start();

    }

    private void fadeOutProgressDialog(){
        mBinding.progressBar.animate().alpha(0f).setDuration(200).start();
    }

    private void delayedStartNextActivity(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), FragmentMainActivity.class));
                finish();
            }
        }, 200);
    }

    private int getFinalWidth(){
        return (int) getResources().getDimension(R.dimen.get_width);
    }
}
