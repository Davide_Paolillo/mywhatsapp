package mobile.uni.mywhatsapp.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.uni.mywhatsapp.R;

public class SettingsActivity extends Fragment {

    private int PICK_IMAGE_INTENT = 1;

    private View view;

    private String uri;

    private CircleImageView mProfileImage;

    private FrameLayout mSetButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_settings, container, false);
        uri = "";
        mProfileImage = view.findViewById(R.id.set_profile_img);
        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });
        mSetButton = view.findViewById(R.id.settingButton);
        mSetButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                addProfileImageToUser();
                startActivity(new Intent(getActivity().getApplicationContext() ,FragmentMainActivity.class));
                getActivity().finish();
            }
        });
        getUserCurrentImage();
        return view;
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*"); // way to say that user should pick an image
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_INTENT);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == getActivity().RESULT_OK){
            if(requestCode == PICK_IMAGE_INTENT){
                if(Objects.requireNonNull(data).getClipData() == null){
                    uri = data.getData().toString();
                    Picasso.get().load(data.getData().toString()).into(mProfileImage);
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Please, pick a single image", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void getUserCurrentImage(){
        DatabaseReference mCurrentUserDb = FirebaseDatabase.getInstance().getReference().child("user").child(FirebaseAuth.getInstance().getUid());
        mCurrentUserDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    if(dataSnapshot.hasChild("profileImg")){
                        Picasso.get().load(dataSnapshot.child("profileImg").getValue().toString()).into(mProfileImage);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void addProfileImageToUser(){
        final DatabaseReference mUserDb = FirebaseDatabase.getInstance().getReference().child("user").child(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
        final HashMap<String, Object> mProfileImageMap = new HashMap<>();
        final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("user").child(FirebaseAuth.getInstance().getUid()).child("profileImg");
        UploadTask uploadTask = filePath.putFile(Uri.parse(uri));
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        mProfileImageMap.put("profileImg", uri.toString());
                        mUserDb.updateChildren(mProfileImageMap);
                    }
                });
            }
        });
    }
}
