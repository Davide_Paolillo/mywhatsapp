package mobile.uni.mywhatsapp.Activities;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.onesignal.OneSignal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import mobile.uni.mywhatsapp.Chat.ChatListAdapter;
import mobile.uni.mywhatsapp.Chat.ChatObject;
import mobile.uni.mywhatsapp.R;
import mobile.uni.mywhatsapp.User.UserObject;
import mobile.uni.mywhatsapp.Utils.DataUtils;

public class LoggedInActivity extends Fragment {

    private RecyclerView.Adapter mChatListAdapter;

    private ArrayList<ChatObject> chatList;

    private FloatingActionButton mDeleteChat, mNewChat;

    private float mDeleteY, mCheckboxX, mCheckBoxStart;

    private View view;

    private boolean cancelData = false;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_loged_in, container, false);
        OneSignalInit();
        Fresco.initialize(Objects.requireNonNull(getActivity()));
        mDeleteChat = view.findViewById(R.id.deleteChat);
        mNewChat = view.findViewById(R.id.newChat);
        chatList = new ArrayList<>();
        mDeleteChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelData = true;
                deleteChatFromDb();
            }
        });
        mNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getApplicationContext(), FindUserActivity.class));
            }
        });
        mDeleteY = 0;
        DataUtils.initContactList(getActivity().getApplicationContext());
        DataUtils.initContactImages(getActivity().getApplicationContext());
        initializeRecyclerView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getUserChatList();
            checkGroups();
            getLastMessage();
            getUsersProfileImages();
        }
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void OneSignalInit() {
        OneSignal.startInit(getActivity()).init();
        OneSignal.setSubscription(true);
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void idsAvailable(String userId, String registrationId) {
                FirebaseDatabase.getInstance().getReference().child("user").child(Objects.requireNonNull(FirebaseAuth.getInstance().getUid())).child("notificationKey").setValue(userId);
            }
        });
        OneSignal.setInFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getUserChatList() {
        DatabaseReference mUserChatDB = FirebaseDatabase.getInstance().getReference().child("user").child(Objects.requireNonNull(FirebaseAuth.getInstance().getUid())).child("chat");
        mUserChatDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        ChatObject mChat = new ChatObject(childSnapshot.getKey());
                        boolean exists = false;
                        for (ChatObject mChatIterator : chatList) {
                            if (mChatIterator.getChatId().equals(mChat.getChatId())) {
                                exists = true;
                            }
                        }
                        if (exists) {
                            continue;
                        }
                        chatList.add(mChat);
                        getChatData(mChat.getChatId());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        setChatName();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setChatName(){
        DatabaseReference mUserChatDB = FirebaseDatabase.getInstance().getReference().child("user").child(Objects.requireNonNull(FirebaseAuth.getInstance().getUid())).child("chat");
        mUserChatDB.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                    final DataSnapshot currentChat = childSnapshot;
                    DatabaseReference mChatUsers = FirebaseDatabase.getInstance().getReference().child("chat").child(childSnapshot.getKey()).child("info").child("users");
                    mChatUsers.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                long i = 2;
                                if(dataSnapshot.getChildrenCount() <= i){
                                    for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                                        if(!childSnapshot.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
                                            DatabaseReference mChatName = FirebaseDatabase.getInstance().getReference().child("user").child(childSnapshot.getKey()).child("phone");
                                            mChatName.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    for(ChatObject mChat : chatList){
                                                        if(dataSnapshot.exists()){
                                                            if(mChat.getChatId().equals(currentChat.getKey())){
                                                                if(!DataUtils.getNameFromPhone(dataSnapshot.getValue().toString()).isEmpty()){
                                                                    mChat.setChatName(DataUtils.getNameFromPhone(dataSnapshot.getValue().toString()));
                                                                } else {
                                                                    mChat.setChatName(dataSnapshot.getValue().toString());
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }
                                } else {
                                    for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                                         DatabaseReference mChatName = FirebaseDatabase.getInstance().getReference().child("user").child(childSnapshot.getKey()).child("phone");
                                         mChatName.addListenerForSingleValueEvent(new ValueEventListener() {
                                             @Override
                                             public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                 for(ChatObject mChat : chatList){
                                                     if(dataSnapshot.exists()){
                                                         if(mChat.getChatId().equals(currentChat.getKey())){
                                                             mChat.setChatName("New Group");
                                                         }
                                                     }
                                                 }
                                             }

                                             @Override
                                             public void onCancelled(@NonNull DatabaseError databaseError) {

                                             }
                                         });
                                    }

                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mChatListAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getChatData(String chatId) {
        DatabaseReference mChatDB = FirebaseDatabase.getInstance().getReference().child("chat").child(Objects.requireNonNull(chatId)).child("info");
        mChatDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String chatId = "";

                    if (dataSnapshot.child("id").getValue() != null) {
                        chatId = dataSnapshot.child("id").getValue().toString();
                    }

                    for (DataSnapshot userSnapshot : dataSnapshot.child("users").getChildren()) {
                        for (ChatObject mChat : chatList) {
                            if (mChat.getChatId().equals(chatId)) {
                                UserObject mUser = new UserObject(userSnapshot.getKey());
                                mChat.addUserToArrayList(mUser);
                                getUserData(mUser);
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void getUserData(UserObject mUser) {
        DatabaseReference mUserDB = FirebaseDatabase.getInstance().getReference().child("user").child(mUser.getUid());
        mUserDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserObject mUser = new UserObject(dataSnapshot.getKey());

                if (dataSnapshot.child("notificationKey").getValue() != null) {
                    mUser.setNotificationKey(dataSnapshot.child("notificationKey").getValue().toString());
                }

                for (ChatObject mChat : chatList) {
                    for (UserObject mUserIt : mChat.getUserObjectArrayList()) {
                        if (mUserIt.getUid().equals(mUser.getUid())) {
                            mUserIt.setNotificationKey(mUser.getNotificationKey());
                        }
                    }
                }
                mChatListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getLastMessage(){
        DatabaseReference mChatList = FirebaseDatabase.getInstance().getReference().child("user").child(FirebaseAuth.getInstance().getUid()).child("chat");
        mChatList.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                    final String chatId = childSnapshot.getKey();
                    DatabaseReference mChat = FirebaseDatabase.getInstance().getReference().child("chat").child(chatId).child("messages");
                    mChat.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                DataSnapshot lastChild = null;
                                for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                                    lastChild = childSnapshot;
                                }
                                if(lastChild.hasChild("text")){
                                    for(ChatObject mChatItem : chatList){
                                        if(mChatItem.getChatId().equals(chatId)){
                                            mChatItem.setLastMsg(lastChild.child("text").getValue().toString(), View.INVISIBLE);
                                        }
                                    }
                                } else {
                                    for(ChatObject mChatItem : chatList){
                                        if(mChatItem.getChatId().equals(chatId)){
                                            mChatItem.setLastMsg("photo", View.VISIBLE);
                                        }
                                    }
                                }
                            } else {
                                for(ChatObject mChatItem : chatList){
                                    if(mChatItem.getChatId().equals(chatId)){
                                        mChatItem.setLastMsg("", View.INVISIBLE);
                                    }
                                }
                            }
                            mChatListAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getUsersProfileImages(){
        final DatabaseReference mUser = FirebaseDatabase.getInstance().getReference().child("user").child(FirebaseAuth.getInstance().getUid()).child("chat");
        mUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                        final String chatId = childSnapshot.getKey();
                        for(final ChatObject mChat : chatList){
                            if(mChat.getChatId().equals(chatId)){
                                DatabaseReference mChatDb = FirebaseDatabase.getInstance().getReference().child("chat").child(chatId).child("info").child("users");
                                mChatDb.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()){
                                            if(dataSnapshot.getChildrenCount() <= 2){
                                                for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                                                    if(!childSnapshot.getKey().equals(FirebaseAuth.getInstance().getUid())){
                                                        DatabaseReference mProfileImg = FirebaseDatabase.getInstance().getReference().child("user").child(childSnapshot.getKey());
                                                        mProfileImg.addValueEventListener(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                if(dataSnapshot.exists()){
                                                                    if(dataSnapshot.hasChild("profileImg")){
                                                                        mChat.setProfileImage(dataSnapshot.child("profileImg").getValue().toString());
                                                                    }
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void checkGroups(){
        DatabaseReference mChatDb = FirebaseDatabase.getInstance().getReference().child("chat");
        mChatDb.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    int i = 0;
                    for(DataSnapshot child : dataSnapshot.getChildren()){
                        for(DataSnapshot childSnapshot : dataSnapshot.child("info").child("users").getChildren()){
                            ++i;
                        }
                        if(i > 2){
                            for(ChatObject mChat : chatList){
                                if(mChat.getChatId().equals(child.getKey())){
                                    mChat.setGroup(true);
                                }
                            }
                        }
                        i = 0;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initializeRecyclerView() {
        RecyclerView mChatList = view.findViewById(R.id.chatList);
        mChatList.setNestedScrollingEnabled(false);
        mChatList.setHasFixedSize(false);
        RecyclerView.LayoutManager mChatListLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayout.VERTICAL, false);
        mChatList.setLayoutManager(mChatListLayoutManager);
        mChatListAdapter = new ChatListAdapter(chatList);
        mChatList.setAdapter(mChatListAdapter);
    }

    /** Graphics */

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_scrolling, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            deleteChatCheckbox(item);
            return true;
        } else if(id == R.id.logout){
            OneSignal.setSubscription(false);
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(Objects.requireNonNull(getActivity()).getApplicationContext(), LogInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void deleteChatCheckbox(MenuItem item) {
        if (item.getTitle().equals("Delete")) {
            item.setTitle("Lock");
            List<CheckBox> checkBoxes = ChatListAdapter.getCheckBoxes();
            for (CheckBox mCheckBox : checkBoxes) {
                handleCheckBoxAnimation(mCheckBox);
            }
            handleDeleteChatAnimation();
        } else {
            item.setTitle("Delete");
            List<CheckBox> checkBoxes = ChatListAdapter.getCheckBoxes();
            for (CheckBox mCheckBox : checkBoxes) {
                handleCheckBoxAnimation(mCheckBox);
            }
            handleDeleteChatAnimation();
        }

    }

    private void handleCheckBoxAnimation(CheckBox checkBox) {
        ObjectAnimator alphaAnimation;
        ObjectAnimator animator;
        if (checkBox.getX() != mCheckBoxStart) {
            mCheckboxX = checkBox.getX();
            mCheckBoxStart = (checkBox.getX() / 1.1f);
        }
        if (checkBox.getAlpha() == 1.0f) {
            animator = ObjectAnimator.ofFloat(checkBox, "x", mCheckBoxStart, mCheckboxX);
            alphaAnimation = ObjectAnimator.ofFloat(checkBox, View.ALPHA, 1.0f, 0.0f);
        } else {
            animator = ObjectAnimator.ofFloat(checkBox, "x", mCheckboxX, mCheckBoxStart);
            alphaAnimation = ObjectAnimator.ofFloat(checkBox, View.ALPHA, 0.0f, 1.0f);
        }
        long animationDuration = 1000;
        alphaAnimation.setDuration(animationDuration);
        animator.setDuration(animationDuration);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator, alphaAnimation);
        animatorSet.start();
    }

    private void handleDeleteChatAnimation() {
        if (mDeleteY == 0) {
            mDeleteY = mDeleteChat.getY();
            mDeleteChat.setY(mNewChat.getY());
        }
        if (mDeleteChat.getAlpha() == 1.0f) {
            mDeleteChat.animate().y(mNewChat.getY()).setDuration(1000).start();
            mDeleteChat.animate().alpha(0.0f).setDuration(1000).start();
        } else {
            mDeleteChat.animate().y(mDeleteY).setDuration(1000).start();
            mDeleteChat.animate().alpha(1.0f).setDuration(1000).start();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void deleteChatFromDb() {
        final DatabaseReference mChatDb = FirebaseDatabase.getInstance().getReference().child("chat");
        final List<CheckBox> checkBoxes = ChatListAdapter.getCheckBoxes();
        mChatDb.addValueEventListener(new ValueEventListener() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(cancelData){
                    int i =0;
                    ArrayList<ChatObject> chatListToDiscard = new ArrayList<>();
                    ArrayList<CheckBox> checkBoxesToDiscard = new ArrayList<>();
                    for(final DataSnapshot chatId : dataSnapshot.getChildren()){
                        if(i < checkBoxes.size()){
                            if(checkBoxes.get(i).isChecked()){
                                for(DataSnapshot mUser : chatId.child("info").child("users").getChildren()){
                                    FirebaseDatabase.getInstance().getReference().child("user").child(mUser.getKey()).child("chat").child(Objects.requireNonNull(chatId.getKey())).removeValue();
                                }
                                for (ChatObject mChat : chatList) {
                                    if (checkBoxes.get(chatList.indexOf(mChat)).isChecked()) {
                                        checkBoxesToDiscard.add(checkBoxes.get(chatList.indexOf(mChat)));
                                        chatListToDiscard.add(mChat);
                                    }
                                }
                            }
                        }
                        ++i;
                    }
                    for(final ChatObject mChat : chatListToDiscard){
                        mChatDb.child(chatList.get(chatList.indexOf(mChat)).getChatId()).removeValue();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                chatList.remove(mChat);
                                mChatListAdapter.notifyDataSetChanged();
                            }
                        }, 500);
                    }
                    chatListToDiscard.clear();
                    for(CheckBox checkBox : checkBoxesToDiscard){
                        checkBoxes.remove(checkBox);
                    }
                    checkBoxesToDiscard.clear();
                }
                cancelData = false;
                mChatListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mChatListAdapter.notifyDataSetChanged();
        view.invalidate();
    }
}
