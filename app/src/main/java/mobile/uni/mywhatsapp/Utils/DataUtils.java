package mobile.uni.mywhatsapp.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

public class DataUtils {

    public static Map<String, String> mUserData;
    public static Map<String, String> mUserImage;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public DataUtils(){
        mUserData = new HashMap<>();
        mUserImage = new HashMap<>();
    }

    public static String getNameFromPhone(String Phone){
        if(mUserData.containsKey(Phone)){
            return mUserData.get(Phone);
        } else {
            return "";
        }
    }

    public static void initContactImages(final Context c){
        for(final String mPhone : mUserData.keySet()){
            DatabaseReference mUserDb = FirebaseDatabase.getInstance().getReference().child("user");
            mUserDb.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                            if(childSnapshot.child("phone").getValue().toString().equals(mPhone)){
                                if(childSnapshot.hasChild("profileImg")){
                                    mUserImage.put(childSnapshot.getKey(), childSnapshot.child("profileImg").getValue().toString());
                                }
                            }
                        }
                        if(dataSnapshot.child(FirebaseAuth.getInstance().getUid()).hasChild("profileImg")){
                            mUserImage.put(FirebaseAuth.getInstance().getUid(), dataSnapshot.child(FirebaseAuth.getInstance().getUid()).child("profileImg").getValue().toString());
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public static String getContactImage(String creatorId){
        if(mUserImage.containsKey(creatorId)){
            return mUserImage.get(creatorId);
        } else {
            return "";
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void initContactList(Context c){
        String ISOPrefix = CountryToPhonePrefixUtils.getCountryISO(c);
        @SuppressLint("Recycle") Cursor phones = c.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while(Objects.requireNonNull(phones).moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phone = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            phone = phone.replace(" ", "");
            phone = phone.replace("-", "");
            phone = phone.replace("(", "");
            phone = phone.replace(")", "");

            if (!String.valueOf(phone.charAt(0)).equals("+")) {
                phone = ISOPrefix + phone;
            }
            mUserData.put(phone, name);
        }
    }

    public static String getPhoneFromeName(String Name){
        if(mUserData.containsValue(Name)){
            for(Iterator<String> mList = mUserData.values().iterator(); mList.hasNext();){
                String it = mList.next();
                if(mUserData.get(it).equals(Name)){
                    return it;
                }
            }
        }
        return "";
    }
}
